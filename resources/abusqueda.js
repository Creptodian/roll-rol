x = [
    {
        "url": "images/Star1.png",
        "titulo": "Un título",
        "generos": "aventura terror",
        "sistema": "d10"
    },
    {
        "url": "images/Star1.png",
        "titulo": "Otro título",
        "generos": "aventura medieval",
        "sistema": "d100"
    },
    {
        "url": "images/Star1.png",
        "titulo": "Este título",
        "generos": "moderno terror",
        "sistema": "d20"
    },
    {
        "url": "images/Star1.png",
        "titulo": "Nuevo título",
        "generos": "anime medieval",
        "sistema": "d6"
    },
    {
        "url": "images/Star1.png",
        "titulo": "Un libro",
        "generos": "fantasía medieval",
        "sistema": "d20"
    },
    {
        "url": "images/Star1.png",
        "titulo": "Este libro",
        "generos": "moderno aventura",
        "sistema": "d10"
    }
]