window.onload = function (e) {

    var availableTags = [
        "ActionScript",
        "AppleScript",
        "Asp",
        "BASIC",
        "C",
        "C++",
        "Clojure",
        "COBOL",
        "ColdFusion",
        "Erlang",
        "Fortran",
        "Groovy",
        "Haskell",
        "Java",
        "JavaScript",
        "Lisp",
        "Perl",
        "PHP",
        "Python",
        "Ruby",
        "Scala",
        "Scheme"
    ];


    $('#star :radio').change(
        function () {
            $('.puntuacion').html(this.value + ' estrellas' + "<br>" + 'Gracias por su voto');
        }
    )

    $("#button").button();

    $('#autocomplete').autocomplete({
        source: availableTags
    });

    $("#tabs").tabs();


    $(document).ready(function () {
        $("#button").click(function () {
            $("#search-bar").slideToggle('slow');
        });

    });

    $("#loginLink").click(function (event) {
        event.preventDefault();
        $(".loginbox").fadeToggle("fast");
    });
    $(".close").click(function () {
        $(".loginbox").fadeToggle("fast");
    });


    $("#iniciosesion").click( function (e) {

        $.ajax({
            dataType: "json",
            url: "js/fakelogin.php",
            Type: "GET",
            data:
            {
                user: $("#usuario").val(),
                pass: $("#password").val(),
            },
            success: function (data){
                if(data[0]!="error"){
                    $("#loginsystem").html("Hola, " + data[0]);
                }
                else{
                    alert("Datos Incorrectos");
                }
            }
        });
    });


}
